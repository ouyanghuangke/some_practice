﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输如你要查看的动物");
            string Name = Console.ReadLine();
           
            if (Name.Equals("狗")) {
                Dog dog = new Dog();
                Console.WriteLine("输入狗的颜色");
                dog.Animalcolor = Console.ReadLine();
                Console.WriteLine("输入狗的名字");
                dog.Animalname = Console.ReadLine();
                dog.Behaviour();
                dog.Move();
            }
            if (Name.Equals("鱼"))
            {
                Fish fish = new Fish();
                Console.WriteLine("输入鱼的颜色");
                fish.Animalcolor = Console.ReadLine();
                Console.WriteLine("输入鱼的名字");
                fish.Animalname = Console.ReadLine();
                fish.Behaviour();
                fish.Move();
            }
            else {
                Console.WriteLine("你输入的有误，请重新启动");
            }
            
          
        }
    }
}
