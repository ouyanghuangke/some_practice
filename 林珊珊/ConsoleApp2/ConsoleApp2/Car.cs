﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Car:Garage
    {
        public string CarName { get; set; }
        public string CarColor { get; set; }
        public int Wheel { get; set; }

        public void Start()
        {
            if (Wheel ==4)
            {
                Console.WriteLine("可以起跑");
            }
            else
            {
                Garage.CarRepair();

                Console.WriteLine("请输入修车厂的店名:");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话号码:");
                GaragePhone = long.Parse(Console.ReadLine());
                Console.WriteLine("请输入修车厂的地址");
                GarageAddress = Console.ReadLine();

            }
        }
    }
}
