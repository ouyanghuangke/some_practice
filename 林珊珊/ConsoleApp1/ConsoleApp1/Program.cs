﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入动物（狗或鱼）");
            string a = Console.ReadLine();

            if (a.Equals("狗"))
            {
                Dog dog = new Dog();
                Console.WriteLine("请输入狗的名字:");
                dog.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入狗的颜色:");
                dog.AnimalColor = Console.ReadLine();
                dog.Bite();
            }

            if (a.Equals("鱼"))
            {
                Fish fish = new Fish();
                Console.WriteLine("请输入鱼的名字:");
                fish.AnimalName = Console.ReadLine();
                Console.WriteLine("请输入鱼的颜色:");
                fish.AnimalColor = Console.ReadLine();
                fish.Blow();
            }
        }
    }
}
