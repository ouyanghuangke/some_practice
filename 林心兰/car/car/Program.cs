﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace car
{
    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car();
            Console.WriteLine("请输入车的名称");
            car.Name = Console.ReadLine();
            Console.WriteLine("请输入车的颜色");
            car.Color = Console.ReadLine();
            Console.WriteLine("请输入车轮数量");
            car.Wheel = int.Parse(Console.ReadLine());

            car.Start();
        }
    }
}
