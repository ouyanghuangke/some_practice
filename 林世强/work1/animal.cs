﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work1
{
   public abstract class animal
    {
        //动物具有名字的属性。
        public string animalname { get; set; }
        //动物具有颜色的属性。
        public string animalcolor { get; set; }
        //动物具有移动的行为。
        public virtual void move()
        {
            Console.WriteLine("动物都具备移动的行为");
        }

    }
}
