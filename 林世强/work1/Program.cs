﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace work1
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog = new Dog();
            dog.bite();
            Fish fish = new Fish();
            fish.spit();
        }
    }
}
