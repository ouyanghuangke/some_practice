﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication3
{
    class Car:Garage 
    {
        public string Carname { get; set; }
        public string CarColor { get; set; }
        public int  Wheel { get; set; }
        public void Running() 
        {
            if (Wheel  >=  4)
            {
                Console.WriteLine("可以起跑！");
            }
            else 
            {
                Garage.Repair();

                Console.WriteLine( "请输入修理厂的名称：");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修理厂的地址：");
                GarageAddress = Console.ReadLine();
                Console.WriteLine("请输入修理厂的电话：");
                GaragePhone = int .Parse(Console .ReadLine ());
            }
            Console.WriteLine("over！");
        }
    }
}
