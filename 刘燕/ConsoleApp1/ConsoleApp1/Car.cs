﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Car 
    {
        public string Carname { get; set; }
        public string Carcolor { get; set; }
        public int Carwheel
        {
            get
            {
                return Carwheel;
            }
            set
            {
                if (value == 4)
            {
               this.run();
            }
            else 
            {
                Garage garage = new Garage();
                garage.repair();
            }
            }
        }

        public void run() 
        {
             Console.WriteLine("可以跑");
        }

    }
}
