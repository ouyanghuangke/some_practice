﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace first_practice
{
    public  abstract class Animal
    {
        public string Name { get; set; }
        public string Color { get; set; }
        public virtual void behavior() 
        {

            Console.WriteLine("移动");
        }
        
        
    }
}
