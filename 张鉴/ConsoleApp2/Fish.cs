﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Fish:Animal
    {
        public string Fname { get; set; }
        public string Fcolor { get; set; }
        public void Blowbubbles()
        {
            Console.WriteLine("鱼的行为：吹泡泡");
            base.move();
        }
    }
}
