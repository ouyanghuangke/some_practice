﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public class Dog : Animal
    {
        public string Dname { get; set; }
        public string Dcolor { get; set; }

        public void Bite()
        {
            Console.WriteLine("狗的行为：咬人");
            base.move();
        }
    }
}
