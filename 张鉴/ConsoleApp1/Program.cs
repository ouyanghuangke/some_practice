﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Car c = new Car();
            Console.WriteLine("请输入车的名字：");
            c.Carname = Console.ReadLine();
            Console.WriteLine("请输入车的颜色：");
            c.Carcolor = Console.ReadLine();
            Console.WriteLine("请输入车的轮子数：");
            c.Carwheels = int.Parse(Console.ReadLine());
            c.Run();
        }
    }
}
