﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
   public  class Car:Garage
    {
        internal int carWheel;

        public string CarName { get; set; }
        public  string  CarColor{ get; set; }
        public int CarWheel { get; set; }
        public string GarageAddress { get; private set; }
        public int GaragePhone { get; private set; }

        public void Run()
        {
            if (CarWheel >=4)
            {
                Console.WriteLine("可以起跑");
            }
            else
            {
                Garage.Repair();
                Console.WriteLine("请输入修车厂的名字：");
                GarageName = Console.ReadLine();
                Console.WriteLine("请输入修车厂的地址：");
                GarageAddress = Console.ReadLine();
                Console.WriteLine("请输入修车厂的电话：");
                GaragePhone = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("结束");
        }
    }
}
