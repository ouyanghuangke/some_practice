﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class Animal
    {
        //名字
        public string Name { get; set; }
        //颜色
        public string Color { get; set; }
        //行为：移动
        public virtual void Move()
        {
            Console.WriteLine("移动");
        }
    }
}
